﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using ld31.Graphics;
using ld31.Entities;
using ld31.Physics;

namespace ld31.Scenes {
    public class MainScene : Scene {

        private List<Entity> entities;
        private Texture2D title;
        private bool renderTitle = true;
        private float titleScale = 1f;

        public Edge TopEdge, RightEdge, BottomEdge, LeftEdge;
        public Camera Camera;


        public MainScene() {
            entities = new List<Entity>();
        }

        public override void Initialize() {
            Camera = new Camera();
            Camera.Scale = 0f;
            
            TopEdge = (Edge)AddEntity(new Edge(new Vector2(Game1.WIDTH*.5f, Edge.START_DEPTH * .5f), EdgeState.Top));
            RightEdge = (Edge)AddEntity(new Edge(new Vector2(Game1.WIDTH - Edge.START_DEPTH*.5f, Game1.HEIGHT*.5f), EdgeState.Right));
            BottomEdge = (Edge)AddEntity(new Edge(new Vector2(Game1.WIDTH * .5f, Game1.HEIGHT - (Edge.START_DEPTH * .5f)), EdgeState.Bottom));
            LeftEdge = (Edge)AddEntity(new Edge(new Vector2(Edge.START_DEPTH*.5f, Game1.HEIGHT * .5f), EdgeState.Left));

            AddEntity(new Paddle(new Vector2(Game1.WIDTH*.5f, Edge.START_DEPTH + Paddle.HEIGHT*.5f), PaddleState.Top));
            AddEntity(new Paddle(new Vector2(Game1.WIDTH - (Edge.START_DEPTH) - Paddle.HEIGHT * .5f, Game1.HEIGHT * .5f), PaddleState.Right));
            AddEntity(new Paddle(new Vector2(Game1.WIDTH * .5f, Game1.HEIGHT - (Edge.START_DEPTH) - Paddle.HEIGHT * .5f), PaddleState.Bottom));
            AddEntity(new Paddle(new Vector2(Edge.START_DEPTH + Paddle.HEIGHT * .5f, Game1.HEIGHT * .5f), PaddleState.Left));
            
        }

        public override void LoadContent() {
            title = Assets.Load<Texture2D>("Title.png");
        }

        public override void UnloadContent() {
            for (int i = 0; i < entities.Count; i++)
                RemoveEntity(entities[i]);
            entities.Clear();
            
            base.UnloadContent();
        }

        public override void Update(GameTime gameTime) {
            float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Camera.Scale += (1 - Camera.Scale) * 1.1f * delta;
            Camera.Update(gameTime);
            for (int i = 0; i < entities.Count; i++)
                if(Camera.Scale >= 0.9f)
                    entities[i].Update(gameTime);
            if (Input.IsMouseButtonPressed(MouseButtons.Left) && renderTitle) {
                AddEntity(new Ball(new Vector2(Game1.WIDTH * .5f, Game1.HEIGHT * .5f)));
                renderTitle = false;
            }

            if (!renderTitle) {
                titleScale += (0 - titleScale) * 1.1f * delta;
            }
        }

        public override void Render(GameTime gameTime, SpriteBatch spriteBatch, GraphicsDevice graphicsDevice) {
            spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.LinearWrap, DepthStencilState.Default, RasterizerState.CullCounterClockwise, null, Camera.Model);
            Shapes.FillRect(spriteBatch, Vector2.Zero, Game1.WIDTH, Game1.HEIGHT, new Color(50, 50, 50));
            for (int i = 0; i < entities.Count; i++)
                entities[i].Render(spriteBatch, gameTime);
            spriteBatch.Draw(title, new Vector2(Game1.WIDTH*.5f, Game1.HEIGHT * .5f), null, Color.White, 0f, new Vector2(title.Width*.5f, title.Height*.5f), titleScale, SpriteEffects.None, 0f);
            spriteBatch.End();
        }

        public Entity AddEntity(Entity entity) {
            entity.ParentScene = this;
            entity.Initialize();
            entities.Add(entity);
            return entity;
        }

        public void RemoveEntity(Entity entity) {
            entities.Remove(entity);
        }

        #region CollisionChecks

        public bool CollisionCheck(Vector2 position, Tag tag) {
            return CollisionCheck(position.X, position.Y, tag);
        }

        public bool CollisionCheck(Vector2 min, Vector2 max, Tag tag) {
            return CollisionCheck(min.X, min.Y, max.X, max.Y, tag);
        }

        public bool CollisionCheck(Collider collider, Tag tag) {
            return CollisionCheck(collider.Min.X, collider.Min.Y, collider.Max.X, collider.Max.Y, tag);
        }

        public bool CollisionCheck(float posX, float posY, Tag tag) {
            return CollisionCheck(posX, posY, posX, posY, tag);
        }

        public bool CollisionCheck(float minX, float minY, float maxX, float maxY, Tag tag) {
            foreach (Entity entity in entities) {
                if (entity.Tags.Contains(tag) && entity.Collider.Intersects(minX, minY, maxX, maxY))
                    return true;
            }
            return false;
        }

        #endregion

        #region CollisionEntity

        public Entity CollisionEntity(Vector2 position, Tag tag) {
            return CollisionEntity(position.X, position.Y, tag);
        }

        public Entity CollisionEntity(Vector2 min, Vector2 max, Tag tag) {
            return CollisionEntity(min.X, min.Y, max.X, max.Y, tag);
        }

        public Entity CollisionEntity(Collider collider, Tag tag) {
            return CollisionEntity(collider.Min.X, collider.Min.Y, collider.Max.X, collider.Max.Y, tag);
        }

        public Entity CollisionEntity(float posX, float posY, Tag tag) {
            return CollisionEntity(posX, posY, posX, posY, tag);
        }

        public Entity CollisionEntity(float minX, float minY, float maxX, float maxY, Tag tag) {
            foreach (Entity entity in entities) {
                if (entity.Tags.Contains(tag) && entity.Collider.Intersects(minX, minY, maxX, maxY))
                    return entity;
            }
            return null;
        }

        #endregion
    }
}
