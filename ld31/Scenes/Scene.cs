﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ld31.Scenes {
    public class Scene {

        protected ContentManager Content {
            get {
                return Program.Game.Content;
            }
        }

        public Scene() {

        }

        public virtual void Initialize() {

        }

        public virtual void LoadContent() {

        }

        public virtual void UnloadContent() {
            Assets.Unload();
            Content.Unload();
        }

        public virtual void Update(GameTime gameTime) {

        }

        public virtual void Render(GameTime gameTime, SpriteBatch spriteBatch, GraphicsDevice graphicsDevice) {

        }
    }
}
