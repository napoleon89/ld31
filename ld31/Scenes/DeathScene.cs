﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Graphics;

namespace ld31.Scenes {
    public class DeathScene : Scene {

        private Texture2D text;

        public DeathScene() : base() {

        }

        public override void LoadContent() {
            text = Assets.Load<Texture2D>("Death.png");
            base.LoadContent();
        }

        public override void Update(GameTime gameTime) {
            if (Input.IsKeyPressed(Keys.Enter)) {
                Program.Game.Scene = new MainScene();
            }
            base.Update(gameTime);
        }

        public override void Render(GameTime gameTime, SpriteBatch spriteBatch, GraphicsDevice graphicsDevice) {
            spriteBatch.Begin();
            spriteBatch.Draw(text, Vector2.Zero, Color.White);
            spriteBatch.End();
        }
    }
}
