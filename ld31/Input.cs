﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ld31 {
    public static class Input {

        private static KeyboardState prevKeyboardState;
        private static GamePadState[] prevGamePadState = new GamePadState[4];
        private static MouseState prevMouseState;
        private static Vector2 prevMousePosition = MousePosition;

        public static void Update() {
            prevKeyboardState = Keyboard.GetState();
            prevMouseState = Mouse.GetState();
            for (int i = 0; i < 4; i++) {
                prevGamePadState[i] = GamePad.GetState((PlayerIndex)i);
            }
            if (MousePosition != prevMousePosition)
                MouseChanged = true;
            else
                MouseChanged = false;
            prevMousePosition = MousePosition;
        }

        #region Keyboard

        public static bool IsKeyDown(Keys key) {
            return Keyboard.GetState().IsKeyDown(key);
        }

        public static bool IsKeyUp(Keys key) {
            return Keyboard.GetState().IsKeyUp(key);
        }

        public static bool IsKeyPressed(Keys key) {
            return Keyboard.GetState().IsKeyDown(key) && prevKeyboardState.IsKeyUp(key);
        }

        public static bool IsKeyReleased(Keys key) {
            return Keyboard.GetState().IsKeyUp(key) && prevKeyboardState.IsKeyDown(key);
        }

        public static float GetKey(Keys key) {
            return Keyboard.GetState().IsKeyDown(key) == true ? 1 : 0;
        }

        #endregion

        #region Mouse

        public static bool IsMouseButtonDown(MouseButtons button) {
            return ButtonToState(button, Mouse.GetState()) == ButtonState.Pressed;
        }

        public static bool IsMouseButtonUp(MouseButtons button) {
            return ButtonToState(button, Mouse.GetState()) == ButtonState.Released;
        }

        public static bool IsMouseButtonPressed(MouseButtons button) {
            return ButtonToState(button, Mouse.GetState()) == ButtonState.Pressed &&
                ButtonToState(button, prevMouseState) == ButtonState.Released;
        }

        public static bool IsMouseButtonReleased(MouseButtons button) {
            return ButtonToState(button, Mouse.GetState()) == ButtonState.Released &&
                ButtonToState(button, prevMouseState) == ButtonState.Pressed;
        }

        public static float GetMouseButton(MouseButtons button) {
            return ButtonToState(button, Mouse.GetState()) == ButtonState.Pressed ? 1 : 0;
        }

        private static ButtonState ButtonToState(MouseButtons button, MouseState mouseState) {
            switch (button) {
                case MouseButtons.Right: return mouseState.RightButton; break;
                case MouseButtons.Left: return mouseState.LeftButton; break;
                case MouseButtons.Middle: return mouseState.MiddleButton; break;
            }
            return ButtonState.Released;
        }

        public static Vector2 MousePosition {
            get {
                Vector2 pos = new Vector2();
                pos.X = Mouse.GetState().Position.X;
                pos.Y = Mouse.GetState().Position.Y;
                return pos;
            }
            set {
                Mouse.SetPosition((int)value.X, (int)value.Y);
            }
        }

        public static bool MouseChanged = false;

        #endregion

        #region GamePad

        public static bool IsGamePadButtonDown(PlayerIndex index, Buttons button) {
            return GamePad.GetState(index).IsButtonDown(button);
        }

        public static bool IsGamePadButtonUp(PlayerIndex index, Buttons button) {
            return GamePad.GetState(index).IsButtonUp(button);
        }

        public static bool IsGamePadButtonPressed(PlayerIndex index, Buttons button) {
            return GamePad.GetState(index).IsButtonDown(button) && prevGamePadState[(int)index].IsButtonUp(button);
        }

        public static bool IsGamePadButtonReleased(PlayerIndex index, Buttons button) {
            return GamePad.GetState(index).IsButtonUp(button) && prevGamePadState[(int)index].IsButtonDown(button);
        }

        public static float GetGamePadButton(PlayerIndex index, Buttons button) {
            return GamePad.GetState(index).IsButtonDown(button) ? 1 : 0;
        }

        public static Vector2 GetGamePadThumbstick(PlayerIndex index, Thumbsticks thumbstick) {
            switch (thumbstick) {
                case Thumbsticks.Left: {
                        return GamePad.GetState(index).ThumbSticks.Left;
                    }
                case Thumbsticks.Right: {
                        return GamePad.GetState(index).ThumbSticks.Right;
                    }
            }
            return Vector2.Zero;
        }

        public static float GetGamePadTrigger(PlayerIndex index, Triggers trigger, float deadzone) {
            switch (trigger) {
                case Triggers.Left: return GamePad.GetState(index).Triggers.Left >= deadzone ? GamePad.GetState(index).Triggers.Left : 0;
                case Triggers.Right: return GamePad.GetState(index).Triggers.Right >= deadzone ? GamePad.GetState(index).Triggers.Right : 0;
            }
            return 0;
        }

        #endregion

    }

    public enum MouseButtons {
        Right,
        Left,
        Middle
    }

    public enum Thumbsticks {
        Left,
        Right
    }

    public enum Triggers {
        Left,
        Right
    }
}
