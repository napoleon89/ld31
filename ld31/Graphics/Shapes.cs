﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ld31.Graphics {
    public class Shapes {

        private static Texture2D pixel;

        public static void Load(GraphicsDevice graphicsDevice) {
            pixel = new Texture2D(graphicsDevice, 1, 1);
            pixel.SetData(new Color[] {Color.White});
        }

        #region FilRect

        public static void FillRect(SpriteBatch spriteBatch, Vector2 position, int width, int height) {
            FillRect(spriteBatch, position, width, height, Color.White);
        }

        public static void FillRect(SpriteBatch spriteBatch, Vector2 position, int width, int height, Color color) {
            FillRect(spriteBatch, position, width, height, 0, Vector2.Zero, color);
        }

        public static void FillRect(SpriteBatch spriteBatch, Vector2 position, int width, int height, float rotation) {
            FillRect(spriteBatch, position, width, height, rotation, new Vector2(.5f, .5f));
        }

        public static void FillRect(SpriteBatch spriteBatch, Vector2 position, int width, int height, float rotation, Color color) {
            FillRect(spriteBatch, position, width, height, rotation, new Vector2(.5f, .5f), color);
        }

        public static void FillRect(SpriteBatch spriteBatch, Vector2 position, int width, int height, float rotation, Vector2 origin) {
            FillRect(spriteBatch, position, width, height, rotation, origin, Color.White);
        }

        public static void FillRect(SpriteBatch spriteBatch, Vector2 position, int width, int height, float rotation, Vector2 origin, Color color) {
            spriteBatch.Draw(pixel, position, null, color, rotation, origin, new Vector2(width, height), SpriteEffects.None, 0f);
        }

        #endregion


    }
}
