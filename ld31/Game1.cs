﻿#region Using Statements
using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
using ld31.Scenes;
using ld31.Graphics;
#endregion

namespace ld31 {
    public class Game1 : Game {

        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private float counterFrame;
        private TimeSpan counterElapsed = TimeSpan.Zero;
        private string windowTitle = "LD31";

        private Scene currentScene;
        private Scene nextScene;

        public Scene Scene {
            get { return currentScene; }
            set { nextScene = value; }
        }

        public static int WIDTH = 1280;
        public static int HEIGHT = 720;

        public Game1()
            : base() {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = WIDTH;
            graphics.PreferredBackBufferHeight = HEIGHT;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize() {
            IsMouseVisible = false;
            Scene = new MainScene();

            base.Initialize();
        }

        protected override void LoadContent() {
            Shapes.Load(GraphicsDevice);
            spriteBatch = new SpriteBatch(GraphicsDevice);
        }

        protected override void UnloadContent() {
            Scene = null;
            Content.Unload();
            Assets.Unload();
        }

        protected override void Update(GameTime gameTime) {
            if (Input.IsGamePadButtonDown(PlayerIndex.One, Buttons.Back) || Input.IsKeyDown(Keys.Escape))
                Exit();

            if (currentScene != null)
                currentScene.Update(gameTime);

            if (currentScene != nextScene) {
                if (currentScene != null)
                    currentScene.UnloadContent();
                currentScene = nextScene;
                GC.Collect();
                GC.WaitForPendingFinalizers();
                if (currentScene != null) {
                    currentScene.Initialize();
                    currentScene.LoadContent();
                }
            }

            Input.Update();

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.Black);

            if (currentScene != null)
                currentScene.Render(gameTime, spriteBatch, GraphicsDevice);

            counterFrame++;
            counterElapsed += gameTime.ElapsedGameTime;
            if (counterElapsed > TimeSpan.FromSeconds(1)) {
                Window.Title = windowTitle + " " + counterFrame.ToString() + " fps - " + (GC.GetTotalMemory(true) / 1048576f).ToString("F") + "MB";
                counterFrame = 0;
                counterElapsed -= TimeSpan.FromSeconds(1);
            }

            base.Draw(gameTime);
        }
    }
}
