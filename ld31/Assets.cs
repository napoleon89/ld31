﻿using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ld31 {
    public class Assets {

        private static Dictionary<string, object> loadedAssets = new Dictionary<string, object>();

        public static string PATH = "Content";

        public static T Load<T>(string path)  {
            if (!loadedAssets.ContainsKey(path)) {
                System.IO.Stream s = System.IO.File.OpenRead(PATH + "/" + path);

                if(typeof(T) == typeof(Texture2D))
                    loadedAssets[path] = Texture2D.FromStream(Program.Game.GraphicsDevice, s);
                

                s.Close();
                s.Dispose();
            }
            return (T)loadedAssets[path];
        }

        public static void Unload() {
            loadedAssets.Clear();
        }
    }
}
