﻿#region Using Statements
using System;
using System.Collections.Generic;
using System.Linq;
#endregion

namespace ld31 {
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        private static Game1 game;

        [STAThread]
        static void Main()
        {
            Game.Run();
        }

        public static Game1 Game {
            get {
                if (game == null)
                    game = new Game1();
                return game;
            }
        }
    }
#endif
}
