﻿using ld31.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ld31.Physics {
    public class Collider {

        public Vector2 Min;
        public Vector2 Max;

        public Vector2 Center {
            get { return (Min + Max) * .5f; }
        }

        public Collider(Vector2 min, Vector2 max) : this(min.X, min.Y, max.X, max.Y) { }

        public Collider(float minX, float minY, float maxX, float maxY) {
            Min = new Vector2(minX, minY);
            Max = new Vector2(maxX, maxY);
        }

        public bool Intersects(Collider other) {
            return Intersects(other.Min.X, other.Min.Y, other.Max.X, other.Max.Y);
        }

        public bool Intersects(Vector2 min, Vector2 max) {
            return Intersects(min.X, min.Y, max.X, max.Y);
        }

        public bool Intersects(float minX, float minY, float maxX, float maxY) {
            if (Min.X < maxX && Max.X > minX && Min.Y < maxY && Max.Y > minY)
                return true;
            return false;
        }

        public void Render(SpriteBatch spriteBatch) {
            Shapes.FillRect(spriteBatch, Min, (int)(Max.X - Min.X), (int)(Max.Y - Min.Y), Color.Red);
        }
    }
}
