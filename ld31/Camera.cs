﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ld31 {
    public class Camera {

        public Vector2 Position = Vector2.Zero;
        public Vector2 TargetPos = Vector2.Zero;
        public float Scale = 1f;

        private Random random = new Random();

        public void Update(GameTime gameTime) {
            float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;
            Position += TargetPos - Position * 10 * delta;
        }

        public void Shake(float amplitude) {
            Shake(amplitude, Mathf.Rand(), Mathf.Rand());
        }

        public void Shake(float amplitude, float dirX, float dirY) {
            Position.X += amplitude * dirX;
            Position.Y += amplitude * dirY;
        }

        public Matrix Model {
            get { return Matrix.CreateTranslation(Position.X, Position.Y, 0) * 
                Matrix.CreateTranslation(-(Position.X + Game1.WIDTH * .5f), -(Position.Y + Game1.HEIGHT * .5f), 0f) * 
                Matrix.CreateScale(Scale) *
                Matrix.CreateTranslation(Position.X + Game1.WIDTH * .5f, Position.Y + Game1.HEIGHT * .5f, 0f)
                ;
            }
        }
    }
}
