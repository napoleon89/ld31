﻿using ld31.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ld31.Entities {
    public class Paddle : Entity {

        public PaddleState State;

        public static int WIDTH = 128;
        public static int HEIGHT = 8;
        
        public Paddle(Vector2 position, PaddleState state) : base(position) {
            AddTag(Tag.Solid);
            this.State = state;
            if (state == PaddleState.Right || state == PaddleState.Left) {
                Width = HEIGHT;
                Height = WIDTH;
            } else {
                Width = WIDTH;
                Height = HEIGHT;
            }

        }

        public override void Update(GameTime gameTime) {
            switch (State) {
                case PaddleState.Top: {
                    Position.X = Input.MousePosition.X;
                    Position.Y = ParentScene.TopEdge.Bottom.Y + Height * .5f;
                    } break;
                case PaddleState.Right: {
                    Position.Y = Input.MousePosition.Y;
                    Position.X = ParentScene.RightEdge.Left.X- Width*.5f;
                    } break;
                case PaddleState.Bottom: {
                    Position.X = Input.MousePosition.X;
                    Position.Y = ParentScene.BottomEdge.Top.Y - Height * .5f;
                    } break;
                case PaddleState.Left: {
                    Position.Y = Input.MousePosition.Y;
                    Position.X = ParentScene.LeftEdge.Right.X + Width*.5f;
                    } break;
            }

            Position.X = MathHelper.Clamp(Position.X, ParentScene.LeftEdge.Right.X + Width * .5f, ParentScene.RightEdge.Left.X - Width * .5f);
            Position.Y = MathHelper.Clamp(Position.Y, ParentScene.TopEdge.Bottom.Y + Height * .5f, ParentScene.BottomEdge.Top.Y - Height * .5f);

            base.Update(gameTime);
        }

        public override void Render(SpriteBatch spriteBatch, GameTime gameTime) {
            Shapes.FillRect(spriteBatch, Position, Width, Height, 0f);
            base.Render(spriteBatch, gameTime);
        }
    }

    public enum PaddleState {
        Top,
        Bottom,
        Left,
        Right
    }
}
