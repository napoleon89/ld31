﻿using ld31.Physics;
using ld31.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ld31.Entities {
    public class Entity {

        public Vector2 Position;
        public List<Tag> Tags;
        public Collider Collider;
        public int Width {
            get {
                return width;
            }
            set {
                width = value;
                if (Collider != null) {
                    Collider.Max.X = Position.X + (value * .5f);
                    Collider.Min.X = Position.X - (value * .5f);
                }
            }
        }
        public int Height {
            get {
                return height;
            }
            set {
                height = value;
                if (Collider != null) {
                    Collider.Max.Y = Position.Y + (value * .5f);
                    Collider.Min.Y = Position.Y - (value * .5f);
                }
            }
        }
        public MainScene ParentScene;

        private int width, height;

        protected float life;

        public Entity(Vector2 position) {
            Position = position;
            Tags = new List<Tag>();
            AddTag(Tag.Entity);
            Width = 16;
            Height = 16;
            Collider = new Collider(Left.X, Top.Y, Right.X, Bottom.Y);
        }

        // Use this if what is being done needs the parent scene
        public virtual void Initialize() { }

        public virtual void Update(GameTime gameTime) {
            life += (float)gameTime.ElapsedGameTime.TotalSeconds;
            UpdateCollider(ref Collider);
        }

        public virtual void UpdateCollider(ref Collider collider) {
            Vector2 delta = Position - collider.Center;
            collider.Min += delta;
            collider.Max += delta;
        }

        public virtual void Render(SpriteBatch spriteBatch, GameTime gameTime) {
            Collider.Render(spriteBatch);
        }

        public Tag AddTag(Tag tag) {
            Tags.Add(tag);
            return tag;
        }

        public void RemoveTag(Tag tag) {
            Tags.Remove(tag);
        }

        public Vector2 Bottom {
            get { return Position + new Vector2(0, Height * .5f); }
        }

        public Vector2 Top {
            get { return Position - new Vector2(0, Height * .5f); }
        }

        public Vector2 Right {
            get { return Position + new Vector2(Width * .5f, 0); }
        }

        public Vector2 Left {
            get { return Position - new Vector2(Width * .5f, 0);  }
        }
    }
}
