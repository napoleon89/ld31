﻿using ld31.Graphics;
using ld31.Scenes;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ld31.Entities {
    public class Edge : Entity {

        public EdgeState State;

        public static int START_DEPTH = 64;

        private Color color;

        public Edge(Vector2 position, EdgeState state) : base(position) {
            State = state;
            AddTag(Tag.Edge);
            AddTag(Tag.Solid);
            if (state == EdgeState.Right || state == EdgeState.Left) {
                Width = START_DEPTH;
                Height = Game1.HEIGHT;
            } else {
                Width = Game1.WIDTH;
                Height = START_DEPTH;
            }
            color = Color.Black;
        }

        public override void Render(SpriteBatch spriteBatch, GameTime gameTime) {
            Shapes.FillRect(spriteBatch, Position, Width, Height, 0f, color);
            color = Color.Black;
        }

        public void Shrink() {
            int amount = 5;
            switch(State) {
                case EdgeState.Top: {
                        Height -= amount;
                        Position.Y -= amount;
                    }break;
                case EdgeState.Right: {
                        Width -= amount;
                        Position.X += amount;
                    } break;
                case EdgeState.Bottom: {
                        Height -= amount;
                        Position.Y += amount;
                    } break;
                case EdgeState.Left: {
                        Width -= amount;
                        Position.X -= amount;
                    } break;
            }
            if (Position.X + Width*.5f <= 0 || Position.X - Width*.5f >= Game1.WIDTH || Position.Y + Height * .5f<= 0 || Position.Y - Height * .5f>= Game1.HEIGHT)
                Program.Game.Scene = new DeathScene();
            color = Color.Red;
        }
        
    }

    public enum EdgeState {
        Top,
        Bottom,
        Left,
        Right
    }
}
