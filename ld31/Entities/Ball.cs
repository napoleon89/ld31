﻿using ld31.Graphics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ld31.Entities {
    public class Ball : Entity {

        private Vector2 velocity;
        private float speed = 300f;
        private Color color = Color.White;
        private float rotation = 0f;
        private Random random = new Random();
        private SoundEffect bounce;
        private SoundEffect paddleBounce;

        public Ball(Vector2 position) : base(position) {
            // Random start velocity
            velocity = new Vector2(speed * Mathf.Rand(), speed * Mathf.Rand());
            Console.WriteLine(velocity.ToString());
            bounce = Program.Game.Content.Load<SoundEffect>("Bounce");
            paddleBounce = Program.Game.Content.Load<SoundEffect>("PaddleBounce");
        }

        public override void Update(GameTime gameTime) {
            float delta = (float)gameTime.ElapsedGameTime.TotalSeconds;
            rotation += 100 * delta;
            if (ParentScene.CollisionEntity(Collider, Tag.Solid) != null) {
                Entity e = ParentScene.CollisionEntity(Collider, Tag.Solid);
                if (e.GetType() == typeof(Paddle)) {
                    Paddle p = e as Paddle;
                    if (p.State == PaddleState.Right || p.State == PaddleState.Left) {
                        velocity.X *= -1;
                        if (Position.Y > p.Position.Y)
                            velocity.Y = Math.Abs(velocity.Y);
                        if (Position.Y < p.Position.Y)
                            velocity.Y = -Math.Abs(velocity.Y);
                    }
                    if (p.State == PaddleState.Top || p.State == PaddleState.Bottom) {
                        velocity.Y *= -1;
                        if (Position.X > p.Position.X)
                            velocity.X = Math.Abs(velocity.X);
                        if (Position.X < p.Position.X)
                            velocity.X = -Math.Abs(velocity.X);
                    }
                    ParentScene.Camera.Shake(2f);
                    paddleBounce.Play();
                }
                if (e.GetType() == typeof(Edge)) {
                    Edge p = e as Edge;
                    ParentScene.Camera.Shake(5f, Math.Sign(velocity.X), Math.Sign(velocity.Y));
                    if (p.State == EdgeState.Right || p.State == EdgeState.Left) {
                        velocity.X *= -1;
                    }
                    if (p.State == EdgeState.Top || p.State == EdgeState.Bottom) {
                        velocity.Y *= -1;
                    }
                    p.Shrink();
                    color = Color.Red;
                    bounce.Play();
                }
            }
            velocity *= 1.0001f;
            Position += velocity * delta;

            
            base.Update(gameTime);
        }

        public override void Render(SpriteBatch spriteBatch, GameTime gameTime) {
            Shapes.FillRect(spriteBatch, Position, Width, Height, rotation, color);
            color = Color.White;
            //base.Render(spriteBatch, gameTime);
        }
    }
}
